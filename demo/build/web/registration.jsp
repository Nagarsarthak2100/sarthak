<%-- 
    Document   : registration
    Created on : 28 Jan, 2019, 9:27:51 PM
    Author     : SARTHAK NAGAR
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registration Page</title>

    </head>
    <body>
        <style type="text/css">
            body
{
    background: url(image/registration-page-background-images-6.jpg);
    background-size: cover;
}
h1
{
    text-align: center;
    color: lime;
}
#login-box
{
    position: relative;
    margin: 5% auto;
    height: 725px;
    width: 700px;
   
    text-align: left;
    font-size: 25px;
    margin-top: 0%;
    color: white;
    border: none;
    outline: none;
}
.box
{
    
    margin-left: 10%;
}
.rightbox
{
    width: 300px;
    height: 400px;
  top: 12%;
  right: 10%;
  position: absolute;
    background-size: cover;
    background-position: center;
    padding: 40px;
    margin-bottom: 20px;
    padding: 4px;
    width: 220px;
    height: 32px;
    border: none;
    outline: none;
    font-family: sans-serif;
    font-weight: 400;
    font-size: 25px;
    transition: 0.2s ease;
      
}
input[type="text"],
input[type="Password"]
{
    display: block;
    box-sizing: border-box;
    margin-bottom: 20px;
    padding: 4px;
    width: 220px;
    height: 32px;
    border: none;
    outline: none;
    border-bottom: 1px solid #aaa;
    font-family: sans-serif;
    font-weight: 400;
    font-size: 15px;
    transition: 0.2s ease;
    border-radius: 1rem;
    
}
button[type="button"]
{
    margin-bottom: 28px;
    width: 120px;
    height: 32px;
    background: green;
    border: none;
    border-radius: 2px;
    color: #fff;
    font-family: sans-serif;
    font-weight: 500;
    text-transform: uppercase;
    transition: 0.2s ease;
    cursor: pointer;
    text-align: center;
    border-radius: 3rem;
    
}
button[type="button"]:hover,
button[type="button"]:focus
{
    background: lime;
    transition: 0.2s ease;
}


input[type="submit"]
{
    margin-bottom: 28px;
    width: 120px;
    height: 32px;
    background: #f44336;
    border: none;
    border-radius: 2px;
    color: #fff;
    font-family: sans-serif;
    font-weight: 500;
    text-transform: uppercase;
    transition: 0.2s ease;
    cursor: pointer;
    text-align: center;
    border-radius: 3rem;
    
}
input[type="submit"]:hover,
input[type="submit"]:focus
{
    background: #ff5722;
    transition: 0.2s ease;
}

input[type="submit"]:hover,
input[type="submit"]:focus
{
    background: #ff5722;
    transition: 0.2s ease;
}
.btn
{
    margin-left: 25%;
    text-align: left;
    margin-top: -3%;
    letter-spacing: 100px;
}   
p
{
    font-size: 15px;
}

        </style>
        <div id="login-box"> 
                   <h1>Registration</h1> 
                   <form action="registration_demo">
                       <div class="box">
                           <div class="leftbox">
                                <input type="text" name="Firstname" placeholder="Enter Your Firstname"><br>
                                <input type="text" name="Middlename" placeholder="Enter Your Middlename"><br>
                                <input type="text" name="Lastname" placeholder="Enter Your Lastname"><br>
                                <input type="text" name="Email" placeholder="Enter Your Email"><br>
                                <input title="Enter your Username" type="text" name="Username" required pattern="\w+" placeholder="Enter Your Username"><br>
                                <input title="Password must contain at least 6 characters, including UPPER/lowercase and numbers" type="password" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" name="Password" onchange="
                                      this.setCustomValidity(this.validity.patternMismatch ? this.title : '');
                                      if(this.checkValidity()) form.Repassword.pattern = RegExp.escape(this.value);
                                    " placeholder="Enter Password"><br>
                                <input title="Please enter the same Password as above" type="password" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" name="Repassword" onchange="
  this.setCustomValidity(this.validity.patternMismatch ? this.title : '');
" placeholder="Please Reenter Password"><br><br>
                           </div>
           
                           <div class="rightbox">
                <input type="text" name="Mobile" placeholder="Enter Your Mobile Number">
                               
                <p style="text-align: center; font-weight: 600;">Gender<input type="radio" name="Gender" value="Male" />Male
                <input type="radio" name="Gender" value="Female"/>Female<br/>
    
                
                <p style="text-align: center; font-weight: 600;">Hobby<br/>
                <input type="checkbox" name="hobby" value="Cricket"/>Cricket<br/>
                <input type="checkbox" name="hobby" value="Singing"/>Singing<br/>
                <input type="checkbox" name="hobby" value="Dancing"/>Dancing<br/>
                <input type="checkbox" name="hobby" value="Reading"/>Reading<br/>
                <input type="checkbox" name="hobby" value="Watch Movie"/>Watch Movie<br/><br/>
                
                Select Your Country:<br/>
                <select name="country" >
                  <option value="India">India</option>
                  <option value="Canada">Canada</option>
                  <option value="Australia">Australia</option>
                  <option value="Japan">Japan</option>
                  <option value="Thailand">Thailand</option>
                </select>
                <p style="color: cadetblue;font-size: 20px;">(/*Password must contain at least 6 characters, including UPPER/lowercase and numbers)</p>               
                           </div>
                       </div>
                       <div class="btn">
                            <input type="submit" name="" value="Sign Up">
                            <a href="login.jsp">
                                <button type="button">Login</button>
                           </a>
                       </div>
                       
                   </form>
            </div>
    </body>
</html>
