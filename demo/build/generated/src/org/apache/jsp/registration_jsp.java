package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class registration_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Registration Page</title>\n");
      out.write("\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <style type=\"text/css\">\n");
      out.write("            body\n");
      out.write("{\n");
      out.write("    background: url(image/registration-page-background-images-6.jpg);\n");
      out.write("    background-size: cover;\n");
      out.write("}\n");
      out.write("h1\n");
      out.write("{\n");
      out.write("    text-align: center;\n");
      out.write("    color: lime;\n");
      out.write("}\n");
      out.write("#login-box\n");
      out.write("{\n");
      out.write("    position: relative;\n");
      out.write("    margin: 5% auto;\n");
      out.write("    height: 725px;\n");
      out.write("    width: 700px;\n");
      out.write("   \n");
      out.write("    text-align: left;\n");
      out.write("    font-size: 25px;\n");
      out.write("    margin-top: 0%;\n");
      out.write("    color: white;\n");
      out.write("    border: none;\n");
      out.write("    outline: none;\n");
      out.write("}\n");
      out.write(".box\n");
      out.write("{\n");
      out.write("    \n");
      out.write("    margin-left: 10%;\n");
      out.write("}\n");
      out.write(".rightbox\n");
      out.write("{\n");
      out.write("    width: 300px;\n");
      out.write("    height: 400px;\n");
      out.write("  top: 12%;\n");
      out.write("  right: 10%;\n");
      out.write("  position: absolute;\n");
      out.write("    background-size: cover;\n");
      out.write("    background-position: center;\n");
      out.write("    padding: 40px;\n");
      out.write("    margin-bottom: 20px;\n");
      out.write("    padding: 4px;\n");
      out.write("    width: 220px;\n");
      out.write("    height: 32px;\n");
      out.write("    border: none;\n");
      out.write("    outline: none;\n");
      out.write("    font-family: sans-serif;\n");
      out.write("    font-weight: 400;\n");
      out.write("    font-size: 25px;\n");
      out.write("    transition: 0.2s ease;\n");
      out.write("      \n");
      out.write("}\n");
      out.write("input[type=\"text\"],\n");
      out.write("input[type=\"Password\"]\n");
      out.write("{\n");
      out.write("    display: block;\n");
      out.write("    box-sizing: border-box;\n");
      out.write("    margin-bottom: 20px;\n");
      out.write("    padding: 4px;\n");
      out.write("    width: 220px;\n");
      out.write("    height: 32px;\n");
      out.write("    border: none;\n");
      out.write("    outline: none;\n");
      out.write("    border-bottom: 1px solid #aaa;\n");
      out.write("    font-family: sans-serif;\n");
      out.write("    font-weight: 400;\n");
      out.write("    font-size: 15px;\n");
      out.write("    transition: 0.2s ease;\n");
      out.write("    border-radius: 1rem;\n");
      out.write("    \n");
      out.write("}\n");
      out.write("button[type=\"button\"]\n");
      out.write("{\n");
      out.write("    margin-bottom: 28px;\n");
      out.write("    width: 120px;\n");
      out.write("    height: 32px;\n");
      out.write("    background: green;\n");
      out.write("    border: none;\n");
      out.write("    border-radius: 2px;\n");
      out.write("    color: #fff;\n");
      out.write("    font-family: sans-serif;\n");
      out.write("    font-weight: 500;\n");
      out.write("    text-transform: uppercase;\n");
      out.write("    transition: 0.2s ease;\n");
      out.write("    cursor: pointer;\n");
      out.write("    text-align: center;\n");
      out.write("    border-radius: 3rem;\n");
      out.write("    \n");
      out.write("}\n");
      out.write("button[type=\"button\"]:hover,\n");
      out.write("button[type=\"button\"]:focus\n");
      out.write("{\n");
      out.write("    background: lime;\n");
      out.write("    transition: 0.2s ease;\n");
      out.write("}\n");
      out.write("\n");
      out.write("\n");
      out.write("input[type=\"submit\"]\n");
      out.write("{\n");
      out.write("    margin-bottom: 28px;\n");
      out.write("    width: 120px;\n");
      out.write("    height: 32px;\n");
      out.write("    background: #f44336;\n");
      out.write("    border: none;\n");
      out.write("    border-radius: 2px;\n");
      out.write("    color: #fff;\n");
      out.write("    font-family: sans-serif;\n");
      out.write("    font-weight: 500;\n");
      out.write("    text-transform: uppercase;\n");
      out.write("    transition: 0.2s ease;\n");
      out.write("    cursor: pointer;\n");
      out.write("    text-align: center;\n");
      out.write("    border-radius: 3rem;\n");
      out.write("    \n");
      out.write("}\n");
      out.write("input[type=\"submit\"]:hover,\n");
      out.write("input[type=\"submit\"]:focus\n");
      out.write("{\n");
      out.write("    background: #ff5722;\n");
      out.write("    transition: 0.2s ease;\n");
      out.write("}\n");
      out.write("\n");
      out.write("input[type=\"submit\"]:hover,\n");
      out.write("input[type=\"submit\"]:focus\n");
      out.write("{\n");
      out.write("    background: #ff5722;\n");
      out.write("    transition: 0.2s ease;\n");
      out.write("}\n");
      out.write(".btn\n");
      out.write("{\n");
      out.write("    margin-left: 25%;\n");
      out.write("    text-align: left;\n");
      out.write("    margin-top: -3%;\n");
      out.write("    letter-spacing: 100px;\n");
      out.write("}   \n");
      out.write("p\n");
      out.write("{\n");
      out.write("    font-size: 15px;\n");
      out.write("}\n");
      out.write("\n");
      out.write("        </style>\n");
      out.write("        <div id=\"login-box\"> \n");
      out.write("                   <h1>Registration</h1> \n");
      out.write("                   <form action=\"registration_demo\">\n");
      out.write("                       <div class=\"box\">\n");
      out.write("                           <div class=\"leftbox\">\n");
      out.write("                                <input type=\"text\" name=\"Firstname\" placeholder=\"Enter Your Firstname\"><br>\n");
      out.write("                                <input type=\"text\" name=\"Middlename\" placeholder=\"Enter Your Middlename\"><br>\n");
      out.write("                                <input type=\"text\" name=\"Lastname\" placeholder=\"Enter Your Lastname\"><br>\n");
      out.write("                                <input type=\"text\" name=\"Email\" placeholder=\"Enter Your Email\"><br>\n");
      out.write("                                <input title=\"Enter your Username\" type=\"text\" name=\"Username\" required pattern=\"\\w+\" placeholder=\"Enter Your Username\"><br>\n");
      out.write("                                <input title=\"Password must contain at least 6 characters, including UPPER/lowercase and numbers\" type=\"password\" required pattern=\"(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,}\" name=\"Password\" onchange=\"\n");
      out.write("                                      this.setCustomValidity(this.validity.patternMismatch ? this.title : '');\n");
      out.write("                                      if(this.checkValidity()) form.Repassword.pattern = RegExp.escape(this.value);\n");
      out.write("                                    \" placeholder=\"Enter Password\"><br>\n");
      out.write("                                <input title=\"Please enter the same Password as above\" type=\"password\" required pattern=\"(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,}\" name=\"Repassword\" onchange=\"\n");
      out.write("  this.setCustomValidity(this.validity.patternMismatch ? this.title : '');\n");
      out.write("\" placeholder=\"Please Reenter Password\"><br><br>\n");
      out.write("                           </div>\n");
      out.write("           \n");
      out.write("                           <div class=\"rightbox\">\n");
      out.write("                <input type=\"text\" name=\"Mobile\" placeholder=\"Enter Your Mobile Number\">\n");
      out.write("                               \n");
      out.write("                <p style=\"text-align: center; font-weight: 600;\">Gender<input type=\"radio\" name=\"Gender\" value=\"Male\" />Male\n");
      out.write("                <input type=\"radio\" name=\"Gender\" value=\"Female\"/>Female<br/>\n");
      out.write("    \n");
      out.write("                \n");
      out.write("                <p style=\"text-align: center; font-weight: 600;\">Hobby<br/>\n");
      out.write("                <input type=\"checkbox\" name=\"hobby\" value=\"Cricket\"/>Cricket<br/>\n");
      out.write("                <input type=\"checkbox\" name=\"hobby\" value=\"Singing\"/>Singing<br/>\n");
      out.write("                <input type=\"checkbox\" name=\"hobby\" value=\"Dancing\"/>Dancing<br/>\n");
      out.write("                <input type=\"checkbox\" name=\"hobby\" value=\"Reading\"/>Reading<br/>\n");
      out.write("                <input type=\"checkbox\" name=\"hobby\" value=\"Watch Movie\"/>Watch Movie<br/><br/>\n");
      out.write("                \n");
      out.write("                Select Your Country:<br/>\n");
      out.write("                <select name=\"country\" >\n");
      out.write("                  <option value=\"India\">India</option>\n");
      out.write("                  <option value=\"Canada\">Canada</option>\n");
      out.write("                  <option value=\"Australia\">Australia</option>\n");
      out.write("                  <option value=\"Japan\">Japan</option>\n");
      out.write("                  <option value=\"Thailand\">Thailand</option>\n");
      out.write("                </select>\n");
      out.write("                <p style=\"color: cadetblue;font-size: 20px;\">(/*Password must contain at least 6 characters, including UPPER/lowercase and numbers)</p>               \n");
      out.write("                           </div>\n");
      out.write("                       </div>\n");
      out.write("                       <div class=\"btn\">\n");
      out.write("                            <input type=\"submit\" name=\"\" value=\"Sign Up\">\n");
      out.write("                            <a href=\"login.jsp\">\n");
      out.write("                                <button type=\"button\">Login</button>\n");
      out.write("                           </a>\n");
      out.write("                       </div>\n");
      out.write("                       \n");
      out.write("                   </form>\n");
      out.write("            </div>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
